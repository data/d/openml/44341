# OpenML dataset: Meta_Album_RSD_Extended

https://www.openml.org/d/44341

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

## **Meta-Album RSD Dataset (Extended)**
***
RSD46 dataset (https://github.com/RSIA-LIESMARS-WHU/RSD46-WHU) is collected from Google Earth and Tianditu. The collection contains 46 scene categories, with a total of 117 000 images. Each scene category has between 500 - 3000 images. The original resolution are 256x256 px or 512x512 px. We have created preprocessed version of RSD for Meta-Album by resizing the original dataset to 128x128 px.  



### **Dataset Details**
![](https://meta-album.github.io/assets/img/samples/RSD.png)

**Meta Album ID**: REM_SEN.RSD  
**Meta Album URL**: [https://meta-album.github.io/datasets/RSD.html](https://meta-album.github.io/datasets/RSD.html)  
**Domain ID**: REM_SEN  
**Domain Name**: Remote Sensing  
**Dataset ID**: RSD  
**Dataset Name**: RSD  
**Short Description**: Remote sensing dataset  
**\# Classes**: 43  
**\# Images**: 43825  
**Keywords**: remote sensing, satellite image, aerial image, land cover  
**Data Format**: images  
**Image size**: 128x128  

**License (original data release)**: Open for research and non-profit purposes  
**License (Meta-Album data release)**: CC BY-NC 4.0  
**License URL (Meta-Album data release)**: [https://creativecommons.org/licenses/by-nc/4.0/](https://creativecommons.org/licenses/by-nc/4.0/)  

**Source**: RSD46-WHU  
**Source URL**: https://github.com/RSIA-LIESMARS-WHU/RSD46-WHU  
  
**Original Author**: Yang Long, Yiping Gong, Zhifeng Xiao, and Qing Liu, Deren Li, Chunshan Wei, Gefu Tang and Junyi Liu  
**Original contact**: longyang@whu.edu.cn  

**Meta Album author**: Phan Anh VU  
**Created Date**: 01 March 2022  
**Contact Name**: Ihsan Ullah  
**Contact Email**: meta-album@chalearn.org  
**Contact URL**: [https://meta-album.github.io/](https://meta-album.github.io/)  



### **Cite this dataset**
```
@ARTICLE{7827088,
  author={Long, Yang and Gong, Yiping and Xiao, Zhifeng and Liu, Qing},
  journal={IEEE Transactions on Geoscience and Remote Sensing}, 
  title={Accurate Object Localization in Remote Sensing Images Based on Convolutional Neural Networks}, 
  year={2017},
  volume={55},
  number={5},
  pages={2486-2498},
  doi={10.1109/TGRS.2016.2645610}}

@article{xiao2017high,
  title={High-resolution remote sensing image retrieval based on CNNs from a dimensional perspective},
  author={Xiao, Zhifeng and Long, Yang and Li, Deren and Wei, Chunshan and Tang, Gefu and Liu, Junyi},
  journal={Remote Sensing},
  volume={9},
  number={7},
  pages={725},
  year={2017},
  publisher={Multidisciplinary Digital Publishing Institute}
}
```


### **Cite Meta-Album**
```
@inproceedings{meta-album-2022,
        title={Meta-Album: Multi-domain Meta-Dataset for Few-Shot Image Classification},
        author={Ullah, Ihsan and Carrion, Dustin and Escalera, Sergio and Guyon, Isabelle M and Huisman, Mike and Mohr, Felix and van Rijn, Jan N and Sun, Haozhe and Vanschoren, Joaquin and Vu, Phan Anh},
        booktitle={Thirty-sixth Conference on Neural Information Processing Systems Datasets and Benchmarks Track},
        url = {https://meta-album.github.io/},
        year = {2022}
    }
```


### **More**
For more information on the Meta-Album dataset, please see the [[NeurIPS 2022 paper]](https://meta-album.github.io/paper/Meta-Album.pdf)  
For details on the dataset preprocessing, please see the [[supplementary materials]](https://openreview.net/attachment?id=70_Wx-dON3q&name=supplementary_material)  
Supporting code can be found on our [[GitHub repo]](https://github.com/ihsaan-ullah/meta-album)  
Meta-Album on Papers with Code [[Meta-Album]](https://paperswithcode.com/dataset/meta-album)  



### **Other versions of this dataset**
[[Micro]](https://www.openml.org/d/44277)  [[Mini]](https://www.openml.org/d/44307)

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/44341) of an [OpenML dataset](https://www.openml.org/d/44341). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/44341/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/44341/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/44341/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

